# Laboratorio 1

Para este laboratorio se pidio resolver dos problemas utilizando metodos greedy.

## Mochila

Cargar una mochila obteniendo la mayor cantidad de beneficio.

### Ejecución

`python3 algoritmo.py ${N_OBJETOS}`

Ejemplo:

`python3 algoritmo.py 500`

## Coloreado

Colorear vertices utilizando la menor cantidad de colores. Los vertices adyacentes no pueden tener un mismo color.

### Ejecución

`python3 algoritmoColoreado.py ${N_VERTICES}`

Ejemplo:

`python3 algoritmoColoreado.py 25`
