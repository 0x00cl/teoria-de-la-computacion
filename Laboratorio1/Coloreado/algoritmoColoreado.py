#!/usr/bin/python
import time
import sys
start_time = time.perf_counter()
file_name = "gcol" + str(sys.argv[1]) + ".txt"
archivo = open(file_name, "r")


def mergeSort(arr): 
    if len(arr) >1: 
        mid = len(arr)//2
        L = arr[:mid]
        R = arr[mid:]
  
        mergeSort(L)
        mergeSort(R)
  
        i = j = k = 0
        while i < len(L) and j < len(R): 
            if len(L[i]['Adyacentes']) > len(R[j]['Adyacentes']): 
                arr[k] = L[i]
                i+= 1
            else: 
                arr[k] = R[j] 
                j+= 1
            k+= 1
        while i < len(L): 
            arr[k] = L[i] 
            i+= 1
            k+= 1
        while j < len(R): 
            arr[k] = R[j] 
            j+= 1
            k+= 1

propVertice = archivo.readline().split()

vertices = [{'color': None, 'Adyacentes': []} for  i in range(int(propVertice[2]))]
colores = [i for  i in range(int(propVertice[2]))]
colorusado = []

for i in archivo:
    eje = i.split()
    vertices[int(eje[1])-1]['Adyacentes'].append(int(eje[2])-1)
    vertices[int(eje[2])-1]['Adyacentes'].append(int(eje[1])-1)

vertices_ordenados_grado = mergeSort(vertices)

coloresAdyacentes = []
for vert in vertices:
    for ady in vert['Adyacentes']:
        color_vertice_adyacente = vertices[ady]['color']
        if color_vertice_adyacente is not None:
            coloresAdyacentes.append(color_vertice_adyacente)
    for color in colores:
        if color not in coloresAdyacentes:
            vert['color'] = color
            colorusado.append(color)
            break
    coloresAdyacentes.clear()

# Imprime por pantalla lista de colores unicos
print(set(colorusado))
print(time.perf_counter() - start_time, "seconds")