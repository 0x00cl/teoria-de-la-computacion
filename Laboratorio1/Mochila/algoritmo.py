#!/usr/bin/python
import time
import sys
start_time = time.perf_counter()
file_name = "knapPI_1_" + str(sys.argv[1]) + "_1000_1"
archivo = open(file_name, "r")
    
MAX_PESO_MOCHILA = 1000
peso_mochila = 0
beneficio_mochila = 0
lista_costos = []
lista_beneficios = []

def mergeSort(arr): 
    if len(arr) >1: 
        mid = len(arr)//2
        L = arr[:mid]
        R = arr[mid:]
  
        mergeSort(L)
        mergeSort(R)
  
        i = j = k = 0
        while i < len(L) and j < len(R): 
            if L[i]['beneficio'] > R[j]['beneficio']: 
                arr[k] = L[i]
                i+= 1
            else: 
                arr[k] = R[j] 
                j+= 1
            k+= 1
        while i < len(L): 
            arr[k] = L[i] 
            i+= 1
            k+= 1
        while j < len(R): 
            arr[k] = R[j] 
            j+= 1
            k+= 1

lista_objetos = []
for objeto in archivo:
    beneficio_costo = objeto.split()
    lista_objetos.append({'beneficio': int(beneficio_costo[0]), 'costo': int(beneficio_costo[1])})

mergeSort(lista_objetos)

i = 0
while i < len(lista_objetos):
    if peso_mochila + lista_objetos[i]['costo'] <= MAX_PESO_MOCHILA:
        peso_mochila += lista_objetos[i]['costo']
        beneficio_mochila += lista_objetos[i]['beneficio']
    i += 1
    
print(peso_mochila)
print(beneficio_mochila)
print(time.perf_counter() - start_time, "seconds")