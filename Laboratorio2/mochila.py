#!/usr/bin/env python3
import sys
import time

nodo = {'nivel': None,
        'beneficio': None,
        'beneficio_max': None,
        'peso': None}


def mergeSort(arr):
    if len(arr) >1:
        mid = len(arr)//2
        L = arr[:mid]
        R = arr[mid:]

        mergeSort(L)
        mergeSort(R)

        i = j = k = 0
        while i < len(L) and j < len(R):
            if (L[i]['beneficio']/L[i]['peso']) > (R[j]['beneficio']/R[j]['peso']):
                arr[k] = L[i]
                i+= 1
            else:
                arr[k] = R[j]
                j+= 1
            k+= 1
        while i < len(L):
            arr[k] = L[i]
            i+= 1
            k+= 1
        while j < len(R):
            arr[k] = R[j]
            j+= 1
            k+= 1


def cota_max(nodo, n, peso, objetos):
    if nodo['peso'] >= peso:
        return 0

    beneficio_max = nodo['beneficio']
    j = nodo['nivel'] + 1
    peso_total = nodo['peso']

    while j < n and peso_total + objetos[j]['peso'] <= peso:
        peso_total += objetos[j]['peso']
        beneficio_max += objetos[j]['beneficio']
        j += 1

    if j < n:
        beneficio_max = beneficio_max + (peso - peso_total) * objetos[j]['beneficio']/objetos[j]['peso']

    return beneficio_max


def mochila(peso, objetos, n):
    lista_nodos = []
    nodo_a = nodo.copy()
    nodo_b = nodo.copy()

    nodo_a['nivel'] = -1
    nodo_a['beneficio'] = 0
    nodo_a['peso'] = 0

    nodo_a_copy = nodo_a.copy()
    lista_nodos.append(nodo_a_copy)

    beneficio_maximo = 0
    while lista_nodos:
        nodo_a = lista_nodos.pop(0)
        if nodo_a['nivel'] == -1:
            nodo_b['nivel'] = 0

        if nodo_a['nivel'] == (n - 1):
            continue

        nodo_b['nivel'] = nodo_a['nivel'] + 1

        nodo_b['peso'] = nodo_a['peso'] + objetos[nodo_b['nivel']]['peso']
        nodo_b['beneficio'] = nodo_a['beneficio'] + objetos[nodo_b['nivel']]['beneficio']

        if nodo_b['peso'] <= peso and nodo_b['beneficio'] > beneficio_maximo:
            beneficio_maximo = nodo_b['beneficio']

        nodo_b['beneficio_max'] = cota_max(nodo_b, n, peso, objetos)

        if nodo_b['beneficio_max'] > beneficio_maximo:
            nodo_b_copy = nodo_b.copy()
            lista_nodos.append(nodo_b_copy)

        nodo_b['peso'] = nodo_a['peso']
        nodo_b['beneficio'] = nodo_a['beneficio']
        nodo_b['beneficio_max'] = cota_max(nodo_b, n, peso, objetos)

        if nodo_b['beneficio_max'] > beneficio_maximo:
            nodo_b_copy = nodo_b.copy()
            lista_nodos.append(nodo_b_copy)

    return beneficio_maximo


file_name = "Mochila/knapPI_1_" + str(sys.argv[1]) + "_1000_1"
archivo = open(file_name, "r")

peso = 30
objetos = []

for objeto in archivo:
    atributos = objeto.split()
    objetos.append({'peso': int(atributos[1]), 'beneficio': int(atributos[0])})

n = len(objetos)

# Comienza calculo del tiempo
start_time = time.perf_counter()

mergeSort(objetos)
beneficio_maximo = mochila(peso, objetos, n)
print(beneficio_maximo)

print(time.perf_counter() - start_time, "seconds")