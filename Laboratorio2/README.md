# Laboratorio 2 - Teoria de la Computación

Para este laboratorio se pidio resolver el problema de la mochila utilizando el metodo de ramificación y acotación.

## Mochila

Cargar una mochila obteniendo la mayor cantidad de beneficio.

### Ejecución

`python3 mochila.py ${N_OBJETOS}`

Ejemplo:

`python3 mochila.py 500`