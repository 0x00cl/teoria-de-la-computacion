# Laboratorio 3

Para este laboratorio se pidio resolver el problema de los puntos más cercanos entre sí utilizando el metodo de division y conquista

## Puntos

Un espacio o matriz con 2 o más puntos, obtener los puntos que están más cerca entre sí.

### Ejecución

`python3 puntos.py ${N_PUNTOS}`

Ejemplo:

`python3 puntos.py 100`
