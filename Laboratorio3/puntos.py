#!/usr/bin/env python3

import time
import sys
import math

def mergeSort(arr, key):
    if len(arr) >1:
        mid = len(arr)//2
        L = arr[:mid]
        R = arr[mid:]

        mergeSort(L, key)
        mergeSort(R, key)

        i = j = k = 0
        while i < len(L) and j < len(R):
            if L[i][key] > R[j][key]:
                arr[k] = L[i]
                i+= 1
            else:
                arr[k] = R[j]
                j+= 1
            k+= 1
        while i < len(L):
            arr[k] = L[i]
            i+= 1
            k+= 1
        while j < len(R):
            arr[k] = R[j]
            j+= 1
            k+= 1


def dist_puntos(p1, p2):
    return math.sqrt(pow(p2["x"] - p1["x"], 2) + pow(p2["y"] - p1["y"], 2))


def fuerzaBruta(puntos, n):
    val_min = float('inf')
    for i in range(n):
        for j in range(i + 1, n):
            if dist_puntos(puntos[i], puntos[j]) < val_min:
                val_min = dist_puntos(puntos[i], puntos[j])
    return val_min


def stripClosest(strip, size, dist_min):
    val_min = dist_min

    for i in range(size):
        j = i + 1
        while j < size and (strip[j]["y"] - strip[i]["y"]) < val_min:
            val_min = dist_puntos(strip[i], strip[j])
            j += 1

    return val_min


def cercanos(puntos, Q, n):
    if n <= 3:
        return fuerzaBruta(puntos, n)

    mitad = n // 2
    puntoMedio = puntos[mitad]

    di = cercanos(puntos[:mitad], Q, mitad)
    dd = cercanos(puntos[mitad:], Q, n - mitad)

    dist_min = min(di, dd)

    strip = []
    for i in range(n):
        if abs(Q[i]["x"] - puntoMedio["x"]) < dist_min:
            strip.append(Q[i])

    return min(dist_min, stripClosest(strip, len(strip), dist_min))


punto = {
    "x": None,
    "y": None,
}

file_name = "Puntos/r_10000_p_" + str(sys.argv[1]) + ".txt"
puntos = []

archivo = open(file_name, "r")
for objeto in archivo:
    atributos = objeto.split()
    coord = punto.copy()
    coord["x"] = int(atributos[0])
    coord["y"] = int(atributos[1])
    puntos.append(coord)
archivo.close()
#===== COMIENZA CALCULO TIEMPO ======#
start_time = time.perf_counter()

n = len(puntos)
mergeSort(puntos, "x")
Q = puntos.copy()
mergeSort(Q, "y")
print("Distancia más cercana es: ", cercanos(puntos, Q, n))

print(time.perf_counter() - start_time, "seconds")
#===== TERMINA CALCULO TIEMPO ======#